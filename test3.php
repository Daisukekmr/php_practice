<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>PHPテスト</title>
</head>
<body>

  <p>文字列の中に変数を展開する練習</p>

  <p>
    <?php
    $text = "文字列表示のテスト";
    echo "$text <br/>";
    $name = '佐藤';
    print "$name さん。こんにちは。";
    print '$name さん。こんにちは。';
    ?>
  </p>

</body>
</html>
