<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title>PHPテスト</title>
</head>
<body>

  <p>文字の表示と文字の連結の練習です</p>

  <p>
    <?php
    $text = '文字列表示のテスト';
    echo $text;
    ?>
    <?php
    $name = '佐藤';
    print $name.'さん。こんにちは。';
    ?>
  </p>

</body>
</html>
